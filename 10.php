<?PHP
    if($_REQUEST){
        $mal=false;
    }else{
        $mal=true;
    }

?>


<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            if($mal){// si mal es verdadero
        ?>
        <div>
            <form name="f">
                <label for="ciudad">
                    Selecciona la ciudad
                </label>
                <select multiple id="ciudad" name="ciudad[]">
                    <optgroup label="Asia">
                        <option value="3">Delhi</option>
                        <option value="4">Hong Kong</option>
                        <option value="8">Mumbai</option>
                        <option value="11">Tokyo</option>
                    </optgroup>
                    <optgroup label="Europe">
                        <option value="1">Amsterdam</option>
                        <option value="5">London</option>
                        <option value="7">Moscow</option>                        
                    </optgroup>
                    <optgroup label="North America">
                        <option value="6">Los Angeles</option>
                        <option value="9">New York</option>                                               
                    </optgroup>
                    <optgroup label="South America">
                        <option value="2">Buenos Aires</option>
                        <option value="10">Sao Paulo</option>                                               
                    </optgroup>
                </select>
                <input type="submit" value="Enviar">     
            </form>            
        </div>        
        <?PHP
            }else{
                // creamos un array con las ciudades
                $ciudades=[
                    0=>"",
                    1=>"Amsterdam",
                    2=>"Buenos Aires",
                    3=>"Delhi",
                    4=>"Hong Kong",
                    5=>"London",
                    6=>"Los Angeles",
                    7=>"Moscow",
                    8=>"Mumbai",
                    9=>"New York",
                    10=>"Sao Paulo",
                    11=>"Tokyo"];               
                
                echo "La ciudad seleccionada:<br>";
                foreach($_REQUEST['ciudad'] as $value){
                    echo "$value-$ciudades[$value]<br>";
                }            
            }
        ?>
    </body>
</html>
